import java.io.*;
import java.util.*;

public class PI{
    
    public static void main(String[] args){
        Scan scan = new Scan();
        int testcases = scan.nextInt();
        while(testcases-- != 0){
            int voters = scan.nextInt();
            int choices = scan.nextInt();
            int[][] chart = new int[voters][choices];
            int[][] pref = new int[voters][choices];
            int[] pointer = new int[voters];
            Arrays.fill(pointer, choices-1);
            for(int i=0;i<voters;i++){
                for(int j=0;j<choices;j++){
                    chart[i][j] = scan.nextInt();
                    pref[i][chart[i][j]] = j;
                }
            }
            State best = null;
            ArrayList<State> list = new ArrayList<>();
            ArrayList<State> next;
            boolean[] b = new boolean[choices];
            Arrays.fill(b, false);
            boolean[] used = new boolean[choices];
            Arrays.fill(used, false);
            int[] cand = new int[2];
            list.add(new State(choices));
            boolean failed = false;
            int unsorted = choices;
            while(unsorted > 1){
                //System.out.println("unsorted = "+unsorted);
                next = new ArrayList<>();
                int bCount = 0;
                for(int i=0;i<voters;i++){
                    int last = chart[i][pointer[i]];
                    if(b[last]) continue;
                    if(bCount == 2){
                        bCount++;
                        break;
                    }
                    cand[bCount] = last;
                    bCount++;
                    b[last] = true;
                }
                if(bCount > 2){
                    failed = true;
                    break;
                }
                //System.out.println("bCount = "+bCount);
                for(State now : list){
                    //System.out.println(now);
                    if(best == null) best = now;
                    else{
                        int cmp = best.compareTo(now);
                        //System.out.println("cmp = "+cmp);
                        if(cmp > 0) continue;
                        if(cmp < 0) best = now;
                    }
                    int l = -1, r = -1;
                    //for(int i=0;i<bCount;i++) System.out.println(cand[i]);
                    if(now.left != -1 && now.right != choices){
                        int lEnd = now.array[now.left];
                        int rEnd = now.array[now.right];
                        for(int i=0;i<voters;i++){
                            int candidate = chart[i][pointer[i]];
                            if(pref[i][rEnd] < pref[i][candidate] && pref[i][candidate] < pref[i][lEnd] && l != candidate){
                                if(l != -1){
                                    failed = true;
                                    break;
                                }
                                l = candidate;
                            }
                            if(pref[i][lEnd] < pref[i][candidate] && pref[i][candidate] < pref[i][rEnd] && r != candidate){
                                if(r != -1){
                                    failed = true;
                                    break;
                                }
                                r = candidate;
                            }
                        }
                    }
                    //System.out.println("l = "+l+", r = "+r+", failed = "+failed);
                    if(failed || (l!=-1 && l==r)) break;
                    int type = bCount - (l==-1 ? 0 : 1) - (r==-1 ? 0 : 1);
                    if(type == 2){
                        next.add(new State(now, cand[0], cand[1]));
                        next.add(new State(now, cand[1], cand[0]));
                    }else if(type == 1){
                        for(int i=0;i<bCount;i++){
                            if(cand[i] != l && cand[i] != r){
                                if(l == -1) next.add(new State(now, cand[i], r));
                                if(r == -1) next.add(new State(now, l, cand[i]));
                            }
                        }
                    }else{
                        next.add(new State(now, l, r));
                    }
                }
                if(failed) break;
                unsorted -= bCount;
                for(int i=0;i<bCount;i++){
                    used[cand[i]] = true;
                    b[cand[i]] = false;
                }
                for(int i=0;i<voters;i++){
                    while(pointer[i]>=0 && used[chart[i][pointer[i]]]) pointer[i]--;
                }
                list = next;
            }
            if(failed){
                System.out.println("Muggles!");
                continue;
            }
            int[] output = null;
            if(unsorted == 1){
                int finish = -1;
                for(int i=0;i<choices;i++){
                    if(used[i]) continue;
                    finish = i;
                    break;
                }
                for(State now : list){
                    now.array[now.left+1] = finish;
                }
            }
            for(State now : list){
                if(output == null){
                    output = now.array;
                    continue;
                }
                for(int i=0;i<choices;i++){
                    if(now.array[i] > output[i]) break;
                    if(now.array[i] < output[i]){
                        output = now.array;
                        break;
                    }
                }
            }
            if(!check(output, chart)) System.out.println("Muggles!");
            else{
                System.out.print(output[0]);
                for(int i=1;i<choices;i++) System.out.print(" "+output[i]);
                System.out.println();
            }
        }   
    }
    
    static boolean check(int[] a, int[][] chart){
        for(int i=0;i<chart.length;i++){
            int peak;
            for(peak=0;peak<chart[i].length;peak++) if(chart[i][0]==a[peak]) break;
            int l = peak-1, r = peak+1;
            for(int j=1;j<chart[i].length;j++){
                if(l > -1 && a[l] == chart[i][j]){
                    l--;
                    continue;
                }
                if(r < a.length && a[r] == chart[i][j]){
                    r++;
                    continue;
                }
                return false;
            }
        }
        return true;
    }
    
    static class State implements Comparable<State>{
        
        int[] array;
        int left, right;
        
        State(int choices){
            array = new int[choices];
            Arrays.fill(array, -1);
            left = -1;
            right = choices;
        }
        
        State(State prev, int l, int r){
            //System.out.println("new state with "+prev+" and "+l+", "+r);
            array = Arrays.copyOf(prev.array, prev.array.length);
            left = prev.left;
            right = prev.right;
            if(l != -1){
                left++;
                array[left] = l;
            }
            if(r != -1){
                right--;
                array[right] = r;
            }
        }
        
        @Override
        public int compareTo(State rhs){
            for(int i=0;i<array.length;i++){
                if(i > rhs.left) return 0;
                if(i > left) return -1;
                if(array[i] == rhs.array[i]) continue;
                return rhs.array[i] - array[i];
            }
            return 0;
        }
        
        @Override
        public String toString(){
            StringBuilder sb = new StringBuilder();
            sb.append("left = ").append(left).append(", right = ").append(right).append('\n');
            for(int in : array) sb.append(in).append(" ");
            return sb.toString();
        }
        
    }
}

class Scan{
    
    BufferedReader buffer;
    StringTokenizer tok;
    
    Scan(){
        buffer = new BufferedReader(new InputStreamReader(System.in));
    }
    
    boolean hasNext(){
        while(tok==null || !tok.hasMoreElements()){
            try{
                tok = new StringTokenizer(buffer.readLine());
            }catch(Exception e){
                return false;
            }
        }
        return true;
    }
    
    String next(){
        if(hasNext()) return tok.nextToken();
        return null;
    }
    
    int nextInt(){
        return Integer.parseInt(next());
    }
    
}
