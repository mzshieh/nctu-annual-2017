#include<bits/stdc++.h>
using namespace std;
int val[100010],len[100010],sz;
int f[10010],b[10010];
void push_x(int x,int k){
	val[sz]=x;
	len[sz]=sz?len[sz-1]:0;
	while(len[sz] && b[len[sz]]!=(x&1)){
		len[sz]=f[len[sz]];
	}
	if(len[sz]||k&&(x&1)==b[0]){
		len[sz]++;
	}
	sz++;
	if(len[sz-1]>=k){
		sz-=k;
	}
}
int main(){
	int T,n,k,x;
	char ins[20];
	scanf("%d",&T);
	for(int i=1;i<=T;i++){
		printf("CASE %d:\n",i);
		scanf("%d%d",&k,&n);
		for(int i=k-1;i>=0;i--){
			getchar();
			b[i]=getchar()-'0';
		}
		for(int i=1;i<k;i++){
			int tmp=f[i];
			while(tmp&&b[i]!=b[tmp]) tmp=f[tmp];
			if(b[i]==b[tmp]) f[i+1]=tmp+1;
			else f[i+1]=0;
		}
		sz=0;
		bool flag=true;
		while(n--){
			scanf("%s",ins);
			if(strcmp(ins,"PUSH")==0){
				scanf("%d",&x);
				if(flag){
					push_x(x,k);
				}
			}
			else if(strcmp(ins,"POP")==0){
				if(flag){
					if(sz){
						printf("%d\n",val[--sz]);
					}
					else{
						puts("ERROR");
						flag=false;
					}
				}
			}
			else if(strcmp(ins,"ADD")==0){
				if(flag){
					if(sz<2){
						puts("ERROR");
						flag=false;
					}
					else{
						int tmp=val[sz-2]+val[sz-1];
						sz-=2;
						push_x(tmp,k);
					}
				}
			}
			else if(strcmp(ins,"EMPTY")==0){
				if(flag){
					sz=0;
				}
			}
			else{
				exit(1);
			}	
		}
	}
	return 0;
}
