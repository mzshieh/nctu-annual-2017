#include<bits/stdc++.h>

using namespace std;

void print(vector<int> &v)
{
	for(int x: v)
		printf(" %d",x);
	puts("");
}

void solve()
{
	vector<int> state, op;
	int n, k, x;
	scanf("%d%d",&k,&n);
	vector<int> pat(k);
	for(int i = 0; i < k; i++)
		scanf("%d",&pat[i]);
	char cmd[16];
	bool failed = false;
	for(int i = 0; i < n; i++)
	{
		scanf("%s",cmd);
//		printf("instruction %d:",i);
//		print(op);
		if(strcmp(cmd,"PUSH")==0)
		{
			scanf("%d",&x);
			state.push_back(x&1);
			op.push_back(x);
			if(op.size()>=k)
			{
				int m = state.size(), j;
				for(j = 0; j < k; j++)
					if(state[m-1-j]!=pat[j])
						break;
				if(j==k)
					while(j--)
					{
						op.pop_back();
						state.pop_back();
					}
			}
		}
		else if(failed)
		{
		}
		else if(strcmp(cmd,"POP")==0)
		{
			if(op.size())
			{
				printf("%d\n",op.back());
				op.pop_back();
			}
			else
			{
				failed=true;
				puts("ERROR");
			}
		}
		else if(strcmp(cmd,"ADD")==0)
		{
			if(op.size()>1)
			{
				int y = op.back();
				op.pop_back();
				state.pop_back();
				op.back() += y;
				state.back() = (op.back() & 1);
				if(op.size()>=k)
				{
					int m = state.size(), j;
					for(j = 0; j < k; j++)
						if(state[m-1-j]!=pat[j])
							break;
					if(j==k)
						while(j--)
						{
							op.pop_back();
							state.pop_back();
						}
				}
			}
			else
			{
				failed=true;
				puts("ERROR");
			}
		}
		else // cmd is "EMPTY"
		{
			while(op.size())
			{
				op.pop_back();
				state.pop_back();
			}
		}
	}
}

int main()
{
	int ncases;
	scanf("%d",&ncases);
	for(int i = 1; i<=ncases;i++)
	{
		printf("CASE %d:\n",i);
		solve();
	}
	return 0;
}
