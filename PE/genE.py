from random import randint as ri
from random import shuffle

def rec(x):
    if x<3: return [ri(0,1)]
    ret = rec((x-1)//2)
    if ri(0,1): return ret+ret+[ri(0,1)]
    return ret+[ri(0,1)]+ret

def gen(T):
    yield [1,0]*5000
    yield ([0]*4999+[1])*2
    for _ in range(20,int(T*0.8)):
        yield rec(10000)
    for _ in range(int(T*0.8),T):
        yield [ri(0,1) for i in range(10000)]

def rnd_insert(lst,limit):
    ret = lst
    while len(ret) < limit:
       pos = ri(len(ret)//2-len(lst)//2+1,len(ret)//2+len(lst)-1)
       ret = ret[:pos] + lst + ret[pos:]
    return ret

print(50)
print(10000,100000)
print(*([0]*9999+[1]))
print(*(['PUSH 6']*99000),sep='\n')
print(*(['ADD']*999+['POP']),sep='\n')
for _ in range(1,18):
    print(10000,100000)
    print(*([0]*9999+[1]))
    print(*(['PUSH 6']*99000),sep='\n')
    instr = ['ADD','ADD','ADD','POP']*250
    shuffle(instr)
    print(*instr,sep='\n')
for b in gen(50):
    print(len(b),100000)
    print(*b)
    b.reverse()
    a = rnd_insert(b,80000)
    for x in a:
        print('PUSH',x+2*ri(-2**30,2**30-2))
    for i in range(len(a),100000):
        if ri(0,2) or i < len(a)+30:
            print('PUSH',ri(-2**31,2**31-1))
        elif ri(0,1):
            print('POP')
        elif i > 99000 and ri(0,9)==0:
            print('EMPTY')
        else:
            print('ADD')
