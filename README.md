# NCTU Annual Programming Contest 2017

For selecting teams representing NCTU in NCPC and ACM-ICPC regionals.

## Problem setters

+	Min-Zheng Shieh

## Keywords

+	A:
+	B:
+	C: Easy geometry
+	D: Easy-Medium Stringology
+	E: Medium Branch-and-Reduce
+	F:
+	G: 
+	H:
+	I:
+	J:
+	K:
+	L:
+   M:
