#include<bits/stdc++.h>
#define N 2020
using namespace std;
struct point{
	double x,y;
	point(){}
	point(double _x,double _y):x(_x),y(_y){}
	point operator-(const point& b)const{
		return point(x-b.x,y-b.y);
	}
	double operator*(const point& b)const{
		return x*b.y-y*b.x;
	}
	double tri(const point &b,const point &c)const{
		return (b-*this)*(c-*this);
	}
}p[N];
int main(){
	int T;
	scanf("%d",&T);
	while(T--){
		int n;
		scanf("%d",&n);
		for(int i=0;i<n;i++){
			scanf("%lf%lf",&p[i].x,&p[i].y);
		}
		for(int i=0;i<n;i++){
			double tmp=p[i].tri(p[(i+1)%n],p[(i+2)%n]);
//			printf("%d %f\n",i,tmp);
			assert(tmp>0);
		}
		double ans=0;
		for(int i=0;i<n;i++){
			int t=(i+2)%n;
			for(int j=(i+1)%n;j!=i;j=(j+1)%n){
				while(t!=i && p[i].tri(p[j],p[(t+1)%n])>p[i].tri(p[j],p[t])){
					t=(t+1)%n;
					//printf("%d\n",t);
				}
				ans=max(ans,p[i].tri(p[j],p[t])*0.5);
			}
		}
		printf("%.15f\n",ans);
	}
	return 0;
}
