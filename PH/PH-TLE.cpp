#include<bits/stdc++.h>

using namespace std;

inline double area(double x, double y, double X, double Y)
{
	return 0.5*fabs(x*Y-X*y);
}

void solve()
{
	int n;
	double x[2002], y[2002];
	scanf("%d",&n);
	for(int i = 0; i < n; i++)
		scanf("%lf%lf",&x[i],&y[i]);
	double ans = 0.0;
	for(int i = 0; i < n; i++)
		for(int j = i+1; j < n; j++)
			for(int k = j+1; k < n; k++)
				ans = max(ans, area(x[i]-x[j],y[i]-y[j],x[i]-x[k],y[i]-y[k]));
	printf("%.20f\n",ans);
}

int main()
{
	int ncases;
	scanf("%d",&ncases);
	while(ncases--)
		solve();
	return 0;
}
