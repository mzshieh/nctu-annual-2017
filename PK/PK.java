import java.lang.*;
import java.io.*;
import java.util.*;

public class PK{
	static long ans[] = new long[1<<20];
	static Scanner sc = new Scanner(System.in);

	static void solve() throws Exception
	{
		int s[] = new int[21];
		int n = sc.nextInt(), m = sc.nextInt(), N = 1 << n;
		for(int i = 0; i < m; i++)
		{
			int x = sc.nextInt(), y = sc.nextInt();
			s[x]|=1<<(y-1);
		}
		for(int i = 1; i < N; i++)
		{
			int p = Integer.bitCount(i);
			ans[i] = 0;
			for(int j = 1; j < N; j <<=1)
				if((j & i)!=0 && (j & s[p])==0)
					ans[i]+=ans[i^j];
		}
		System.out.println(ans[N-1]);
	}
	public static void main(String args[]) throws Exception
	{
		ans[0] = 1;
		int ncases = sc.nextInt();
		while(ncases-->0) solve();
	}
}
