import java.io.*;
import java.util.*;

public class PJ{
    
    static int n, m;
    static char[][] input;
    static final int[] minDegree = new int[]{2, 0};
    static final int[] maxDegree = new int[]{2, 8};
    static final int[] spaceSize = new int[]{9, 27, 81, 243, 729, 2187, 6561, 19683, 59049, 177147, 531441, 1594323};
    
    public static void main(String[] args){
        Scan scan = new Scan();
        int testcases = scan.nextInt();
        while(testcases-- != 0){
            n = scan.nextInt();
            m = scan.nextInt();
            input = new char[n][];
            for(int i=0;i<n;i++) input[i] = scan.next().toCharArray();
            int size = spaceSize[m-1];
            int[] last = new int[size];
            int[] now = new int[size];
            int[] border = new int[m+1];
            border[0] = 1;
            Arrays.fill(now, -1);
            now[encode(border)] = 0;
            for(int i=0;i<n;i++){
                for(int j=0;j<m;j++){
                    int[] buffer = last;
                    last = now;
                    now = buffer;
                    Arrays.fill(now, -1);
                    for(int k=0;k<size;k++){
                        if(last[k] == -1) continue;
                        border = decode(k, border);
                        int degree = border[j] + border[m];
                        int selector = input[i][j] == '+' ? 1 : 0;
                        int min = minDegree[selector], max = maxDegree[selector];
                        if(degree > max) continue;
                        for(int a=0;a<3;a++){
                            for(int b=0;b<3;b++){
                                if(b != 0 && j == m-1) continue;
                                int newDegree = degree + a + b;
                                if(newDegree < min || newDegree > max) continue;
                                if(((degree + a + b)&1) > 0) continue;
                                border[j] = a;
                                border[m] = b;
                                int newCount = last[k] + (newDegree == 0 ? 0 : 1);
                                int newState = encode(border);
                                now[newState] = now[newState] == -1 ? newCount : Math.min(newCount, now[newState]);
                            }
                        }
                    }
                }
            }
            Arrays.fill(border, 0);
            border[m-1] = 1;
            int endState = encode(border);
            if(now[endState] != -1) System.out.println(now[endState]);
            else System.out.println("impossible");
        }
    }
    
    static int encode(int[] border){
        int result = 0;
        for(int i=0;i<=m;i++){
            result *= 3;
            result += border[i];
        }
        return result;
    }
    
    static int[] decode(int code, int[] border){
        for(int i=m;i>=0;i--){
            border[i] = code % 3;
            code /= 3;
        }
        return border;
    }
    
}

class Scan{
    
    BufferedReader buffer;
    StringTokenizer tok;
    
    Scan(){
        buffer = new BufferedReader(new InputStreamReader(System.in));
    }
    
    boolean hasNext(){
        while(tok==null || !tok.hasMoreElements()){
            try{
                tok = new StringTokenizer(buffer.readLine());
            }catch(Exception e){
                return false;
            }
        }
        return true;
    }
    
    String next(){
        if(hasNext()) return tok.nextToken();
        return null;
    }
    
    int nextInt(){
        return Integer.parseInt(next());
    }
    
}
