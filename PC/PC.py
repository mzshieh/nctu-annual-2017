import math

def f(w,h,W,H):
    if w >= W and h >= H: return 'Y'
    d2 = w*w+h*h
    D2 = W*W+H*H
    if w*w >= D2: return 'Y'
    if h*h >= D2: return 'Y' if w >= W else 'N'
    if d2 < D2: return 'N'
    d, D = d2**0.5, D2**0.5
    theta = (math.asin(w/D)+math.asin(h/D)-math.pi/2)/2
    if W <= D*math.sin(theta)*(1+1e-7):
        return 'Y'
    return 'N'

T = int(input())
for i in range(T):
    w, h, W, H = [int(x) for x in input().strip().split()]
    w, h, W, H = min(w,h),max(w,h),min(W,H),max(W,H)
    print(f(w,h,W,H),f(W,H,w,h),sep='')
