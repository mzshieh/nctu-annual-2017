import math

def f(w,h,W,H):
    return 'Y' if w >= W and h >= H else 'N'

T = int(input())
for i in range(T):
    w, h, W, H = [int(x) for x in input().strip().split()]
    w, h, W, H = min(w,h),max(w,h),min(W,H),max(W,H)
    print(f(w,h,W,H),f(W,H,w,h),sep='')
