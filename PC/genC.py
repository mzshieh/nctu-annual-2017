from random import randint as ri

def gen(T):
    yield 1,2,2,1
    yield 30,40,49,9
    yield 500,500,700,7
    yield 1000,10,11,11
    for _ in range(4,T):
        yield ri(1,1000),ri(1,1000),ri(1,1000),ri(1,1000)

print(10000)
for cases in gen(10000):
    print(*cases)
