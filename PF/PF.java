import java.io.*;
import java.util.*;

public class PF{
    
    public static void main(String[] args){
        Scan scan = new Scan();
        int testcases = scan.nextInt();
        while(testcases-- != 0){
            int n = scan.nextInt();
            boolean[][] chart = new boolean[n][n];
            int[] outDegree = new int[n];
            int maxOut = 0, maxValue = 0;
            for(int i=0;i<n;i++){
                for(int j=0;j<n;j++){
                    chart[i][j] = scan.nextInt()==1;
                    outDegree[i] += chart[i][j]? 1 : 0;
                }
                if(outDegree[i] > maxValue){
                    maxValue = outDegree[i];
                    maxOut = i;
                }
            }
            boolean[] seen = new boolean[n];
            Queue<Integer> queue = new ArrayDeque<>();
            int counter = 0;
            queue.add(maxOut);
            while(!queue.isEmpty()){
                int now = queue.poll();
                if(seen[now]) continue;
                seen[now] = true;
                counter++;
                for(int i=0;i<n;i++){
                    if(!chart[i][now]) continue;
                    queue.add(i);
                }
            }
            System.out.println(counter);
        }
    }
    
}

class Scan{
    
    BufferedReader buffer;
    StringTokenizer tok;
    
    Scan(){
        buffer = new BufferedReader(new InputStreamReader(System.in));
    }
    
    boolean hasNext(){
        while(tok==null || !tok.hasMoreElements()){
            try{
                tok = new StringTokenizer(buffer.readLine());
            }catch(Exception e){
                return false;
            }
        }
        return true;
    }
    
    String next(){
        if(hasNext()) return tok.nextToken();
        return null;
    }
    
    int nextInt(){
        return Integer.parseInt(next());
    }
    
}
