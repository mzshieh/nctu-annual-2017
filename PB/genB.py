from random import randint as ri

def gen(T):
    yield 0,[]
    yield 5,[1,2,3,4,5]
    yield 10000,[ri(1,202) for x in range(10000)]
    for _ in range(T):
        m = ri(1,202)
        cand = [ri(1,202) for x in range(m+1)]
        yield 10000,[cand[ri(0,m)] for x in range(10000)]

print(100)
for n, cases in gen(100):
    print(n)
    print(*cases)
