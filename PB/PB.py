T = int(input())
for i in range(T):
    input()
    f = set()
    for x in input().split():
        if x=='1': continue
        f.add(int(x))
    f = list(f)
    f.sort(reverse=True)
    ans = 0
    while len(f) > 0:
        if f[-1]+1 in f:
            f.pop()
        f.pop()
        ans += 1
    print(ans)
