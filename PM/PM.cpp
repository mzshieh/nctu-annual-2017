#include <stdio.h>

#include <vector>
#include <algorithm>
#include <queue>

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;

#define MAXN 5005

const ll MAX = (ll)1e15;

int n,m,q;

ll g[MAXN];
vector<pii> edge[MAXN];

ll dis[MAXN];
int inq[MAXN];
int vst[MAXN];

ll SPFA(int start, int init)
{
	dis[start] = init;
	queue<int> que;
	que.push(start);
	inq[start]=1;
	while(!que.empty()){
		int wh = que.front();
		que.pop();
		inq[wh] = 0;
		for(size_t i=0; i<edge[wh].size(); i++){
			pii &tar = edge[wh][i];
			if(dis[tar.first] < dis[wh]+tar.second){
				dis[tar.first] = dis[wh]+tar.second;
				if(inq[tar.first] == 0){
					que.push(tar.first);
					inq[tar.first] = 1;
					vst[tar.first]++;
					if(vst[tar.first]>n){
						return -1;
					}
				}
			}
		}
	}
	if(dis[n]==-1){
		return -2;
	}else{
		return dis[n]+g[n];
	}
}

void dfs(int wh)
{
	vst[wh] = 1;
	for(size_t i=0; i<edge[wh].size(); i++)
		if(vst[edge[wh][i].first] == 0)
			dfs(edge[wh][i].first);
}

int main()
{
	int cas;
	scanf("%d",&cas);
	while(cas--){
		scanf("%d %d %d",&n,&m,&q);
		for(int i=1; i<=n; i++){
			scanf("%lld",&g[i]);
			edge[i].clear();
			dis[i] = -1;
			inq[i] = 0;
			vst[i] = 0;
		}
		for(int i=0; i<m; i++){
			int u,v,p;
			scanf("%d %d %d",&u,&v,&p);
			edge[u].push_back(make_pair(v,g[u]-p));
			edge[v].push_back(make_pair(u,g[v]-p));
		}
		dfs(1);
		if(vst[n] == 0){
			printf("-2\n");
			continue;
		}
		for(int i=1; i<=n; i++){
			vst[i] = 0;
		}
		printf("%lld\n",SPFA(1,q));
	}
	return 0;
}
