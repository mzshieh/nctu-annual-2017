T = int(input())
print(T)
for _ in range(T):
    n, m,q = [int(x) for x in input().strip().split()]
    g = [int(x) for x in input().strip().split()]
    cand = [tuple(int(y) for y in input().strip().split()) for x in range(m)]
    edge = {}
    for c in cand:
        if c[0]==c[1]: continue
        edge.setdefault(c[:2],0)
        edge[c[:2]] = max(edge[c[:2]],c[2])
    m = len(edge)
    print(n,m,q)
    print(*g)
    for k, e in edge.items():
        print(k[0],k[1],e)
