#include<bits/stdc++.h>
#define N 1010
#define M 100100
using namespace std;
int t[N];
long long dis[N];
struct edge{
	int x;
	int p;
}e[M*2];
vector<edge> g[N];
int main(){
	int T,n,m,q,u,v,w;
	scanf("%d",&T);
	assert(T<=10);
	while(T--){
		set<pair<int,int> > s;
		scanf("%d%d%d",&n,&m,&q);
		if(n>1000||m>100000) fprintf(stderr,"!! %d %d %d\n",n,m,q);
		assert(n<=1000&&m<=100000);
		assert(q<=10000000);
		for(int i=1;i<=n;i++) scanf("%d",&t[i]),assert(t[i]<=10000000);
		for(int i=0;i<m;i++){
			scanf("%d%d%d",&u,&v,&w);
			if(u>v) swap(u,v);
			assert(u<v);
			assert(u>=1&&v<=n);
			//if(s.count(make_pair(u,v))) fprintf(stderr,"%d %d %d\n %d %d\n",n,m,q,u,v);
			//assert(!s.count(make_pair(u,v)));
			//s.insert(make_pair(u,v));
			e[i*2].p=e[i*2+1].p=w;
			e[i*2].p-=t[u];
			e[i*2+1].p-=t[v];
			e[i*2].x=v;
			e[i*2+1].x=u;
			g[u].push_back(e[i*2]);
			g[v].push_back(e[i*2+1]);
		}
		for(int i=2;i<=n;i++) dis[i]=1;
		dis[1]=-q;
		bool flag;
		for(int i=0;i<n+100;i++){
			flag=false;
			for(u=1;u<=n;u++){
				for(int j=0;j<g[u].size();j++){
					v=g[u][j].x;
					if(dis[u]+g[u][j].p<dis[v]){
						dis[v]=dis[u]+g[u][j].p;
						flag=true;
					}
				}
			}
		}
		if(flag) puts("-1");
		else if(dis[n]>0) puts("-2");
		else printf("%lld\n",-dis[n]+t[n]);
		for(int i=1;i<=n;i++) g[i].clear();
	}
	return 0;
}