#include <stdio.h>
#include <stdlib.h>

#include <set>
#include <vector>

#include <algorithm>

using namespace std;

typedef pair<pair<int,int>,int> piii;

int mrand()
{
	return ((rand()<<15) | rand()) & 0x7fffffff;
}

int main()
{
	int cas;
	int n,m,q;
	int cntn,cntm;
	cas = 10;
	printf("%d\n",cas);
	{
		n=1000;
		m=1498;
		q=10000000;
		cntn = cntm = 0;
		printf("%d %d %d\n", n, m, q);
		for(int i=1; i<999; i+=2){
			printf("%d %d ", 10, 80);
			cntn+=2;
		}
		printf("%d %d\n",10,10);
		cntn+=2;
		for(int i=1; i<999; i+=2){
			printf("%d %d %d\n", i, i+2, 50);
			printf("%d %d %d\n", i, i+1, 60);
			printf("%d %d %d\n", i+1, i+2, 60);
			cntm+=3;
		}
		printf("%d %d %d\n", 999, 1000, 100);
		cntm+=1;
		if(n!=cntn || m!=cntm){
			fprintf(stderr, "testcase 1 fail\n");
		}
		fprintf(stderr,"case %d: %d %d %d\n",1, n,m,q);
	}
	for(int T=2; T<=3; T++){
		n=1000;
		m=100000;
		q=2;
		cntn = cntm = 0;
		printf("%d %d %d\n", n, m, q);
		vector<int> gvec;
		for(int i=1; i<=n; i++){
			gvec.push_back(2);
			cntn+=1;
		}
		for(size_t i=0; i<gvec.size(); i++)
			printf("%d%c",gvec[i],i+1==gvec.size()?'\n':' ');

		for(int i=2; i<n; i++){
			printf("%d %d %d\n", 1, i, 1);
			cntm+=1;
		}
		if(T%2==0){
			printf("1 %d 10000000\n", n);
			cntm+=1;
		}
		vector<pair<int,int> > vec;
		for(int i=2; i<n; i++){
			for(int j=i+1; j<n; j++){
				vec.push_back(make_pair(i,j));
			}
		}
		while((int)vec.size()>m-(n-(T%2==0?1:2))){
			int tar=mrand()%vec.size();
			swap(vec[tar], vec.back());
			vec.pop_back();
		}
		random_shuffle(vec.begin(), vec.end());
		for(int i=0; i<m-(n-(T%2==0?1:2)); i++){
			if(mrand()&1){
				printf("%d %d %d\n", vec[i].first, vec[i].second, mrand()%1000000+3);
			}else{
				printf("%d %d %d\n", vec[i].second, vec[i].first, mrand()%1000000+3);
			}
			cntm+=1;
		}
		if(n!=cntn || m!=cntm){
			fprintf(stderr, "testcase 2 fail %d %d %d %d\n",n,cntn,m,cntm);
		}
		fprintf(stderr,"case %d: %d %d %d\n",T, n,m,q);
	}
/*
3 2 5
6 8 7
1 2 7
1 3 11
*/
	for(int T=4; T<=5; T++){
		n=1000;
		m=100000;
		q=8698251;
		cntn = cntm = 0;
		vector<int> gvec;
		vector<piii> evec;
		gvec.resize(n);
		for(int i=0; i<n; i++)
			gvec[i] = (i%100==98?1000:70);
		cntn+=gvec.size();
		
		for(int i=99; i<n; i+=100){
			for(int j=i+100; j<n; j++){
				evec.push_back(make_pair(make_pair(i,j),1000));
				cntm+=1;
			}
		}
		for(int i=1; i<n; i++){
			int tmpm = mrand()%(n-i)/3+1;
			if(i%100 == 99)
				tmpm-=(n-i)/100;
			if(tmpm<1)
				tmpm = 1;
			vector<int> tmpvec;
			for(int j=i+1; j<=n; j++){
				if(i%100 == 99 && j%100 == 99) continue;
				tmpvec.push_back(j);
			}
			while(tmpm<(int)tmpvec.size()){
				int tar=mrand()%tmpvec.size();
				swap(tmpvec[tar], tmpvec.back());
				tmpvec.pop_back();
			}
			sort(tmpvec.begin(), tmpvec.end());
			for(size_t j=0; j<tmpvec.size(); j++){
				evec.push_back(make_pair(make_pair(i,tmpvec[j]),max((n-i+6)*100-j,(size_t)80)));
				cntm+=1;
			}
		}
		m=cntm;
		printf("%d %d %d\n", n, m, q);
		for(size_t i=0; i<gvec.size(); i++)
			printf("%d%c",gvec[i],i+1==gvec.size()?'\n':' ');
		for(size_t i=0; i<evec.size(); i++)
			printf("%d %d %d\n",evec[i].first.first,evec[i].first.second,evec[i].second);
		if(cntm>100000)
			fprintf(stderr,"testcase %d too many %d\n", T, cntm);
		if(n!=cntn || m!=cntm){
			fprintf(stderr, "testcase 2 fail\n");
		}
		fprintf(stderr,"case %d: %d %d %d\n",T, n,m,q);
	}
	for(int T=6; T<=cas-1; T++){
		n=1000;
		m=100000;
		q=598251;
		cntn = cntm = 0;
		vector<int> gvec;
		vector<piii> evec;
		gvec.resize(n);
		for(int i=0; i<n; i++)
			gvec[i] = (i%100==2?(i/100%2==1?10000000:90):80);
		cntn+=gvec.size();
		
		for(int i=2; i<n; i+=200){
			evec.push_back(make_pair(make_pair(i,i+100), 4000000));
			cntm+=1;
		}
		for(int i=1; i<n; i++){
			if(i%200 == 103) continue;
			int tmpm = mrand()%(n-i)/3;
			vector<int> tmpvec;
			for(int j=i+1; j<=n; j++){
				if(j%200 == 103) continue;
				tmpvec.push_back(j);
			}
			while(tmpm<(int)tmpvec.size()){
				int tar=mrand()%tmpvec.size();
				swap(tmpvec[tar], tmpvec.back());
				tmpvec.pop_back();
			}
			sort(tmpvec.begin(), tmpvec.end());
			for(size_t j=0; j<tmpvec.size(); j++){
				evec.push_back(make_pair(make_pair(i,tmpvec[j]),max((n-i+6)*100-j,(size_t)95)));
				cntm+=1;
			}
		}
		if(T%2==0){
			for(size_t i=0; i<evec.size(); i++){
				if(evec[i].first.second==n)
					evec[i].second = 500000;
			}
		}
		m=cntm;
		printf("%d %d %d\n", n, m, q);
		for(size_t i=0; i<gvec.size(); i++)
			printf("%d%c",gvec[i],i+1==gvec.size()?'\n':' ');
		for(size_t i=0; i<evec.size(); i++)
			printf("%d %d %d\n",evec[i].first.first,evec[i].first.second,evec[i].second);
		if(cntm>100000)
			fprintf(stderr,"testcase %d too many %d\n", T, cntm);
		if(n!=cntn || m!=cntm){
			fprintf(stderr, "testcase 2 fail\n");
		}
		fprintf(stderr,"case %d: %d %d %d\n",T, n,m,q);
	}
	for(int T=cas; T<=cas; T++){
		n = mrand()%999+2;
		m = mrand()%99999+2;
		q = mrand()%10000000+1;
		if(m > n*(n-1)/2)
			m = n*(n-1)/2;
		printf("%d %d %d\n",n,m,q);
		for(int i=0; i<n; i++){
			printf("%d%c", mrand()%10000000+1, i+1==n?'\n':' ');
		}
		vector<pair<int,int> > vec;
		for(int i=1; i<n; i++){
			for(int j=i+1; j<=n; j++){
				vec.push_back(make_pair(i,j));
			}
		}
		while((int)vec.size()>m){
			int tar=mrand()%vec.size();
			swap(vec[tar], vec.back());
			vec.pop_back();
		}
		random_shuffle(vec.begin(), vec.end());
		for(int i=0; i<m; i++){
			if(mrand()&1)
				printf("%d %d %d\n", vec[i].first, vec[i].second, mrand()%10000000+1);
			else
				printf("%d %d %d\n", vec[i].second, vec[i].first, mrand()%10000000+1);
		}
		fprintf(stderr,"case %d: %d %d %d\n",T, n,m,q);
	}
	return 0;
}
