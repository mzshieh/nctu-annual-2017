T = int(input())
for i in range(T):
    n = int(input())
    print('PCCA' if n * 50 * 120 > 90000 else 'KFCCA')
