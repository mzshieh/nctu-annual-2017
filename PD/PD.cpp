#include<bits/stdc++.h>

using namespace std;

char in[102400];

void solve()
{
	queue<int> L, O, V;
	int ans = 0;
	scanf("%s",in);
	for(int i = 0; in[i]; i++)
	{
		if(in[i]=='L') L.push(i);
		else if(in[i]=='O') O.push(i);
		else if(in[i]=='V') V.push(i);
		else
		{
			if(L.empty() || O.empty() || V.empty()) continue;
			while(!O.empty() && L.front() > O.front()) O.pop();
			if(O.empty()) continue;
			while(!V.empty() && O.front() > V.front()) V.pop();
			if(V.empty()) continue;
			L.pop(); O.pop(); V.pop(); ans++;
		}
	}
	printf("%d\n",ans);
}

int main()
{
	int ncases;
	scanf("%d",&ncases);
	while(ncases--)
		solve();
	return 0;
}
