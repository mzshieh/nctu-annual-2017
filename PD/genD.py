#!/usr/bin/env python3

from random import randint as ri

def cases(n):
    yield 'LLLOOOEEEVVVOOOLLLVVVEEEEEE'
    for _ in range(1,n):
         yield ''.join('LOVE'[ri(0,3)] for __ in range(100000))

print(100)
print(*cases(100),sep='\n')
